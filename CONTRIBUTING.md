# Contributing

When contributing to this repository, please first discuss the change you wish to make via issue,
email, or any other method with the owners of this repository before making a change.

## Pull Request Process

1. Ensure any install or build dependencies are removed before the end of the layer when doing a
   build.
2. Follow the standards and conventions set forth in other project documentation.
3. Update the README.md with details of changes to the index, this includes the addition or removal
   of documents and changes to titles, descriptions or other metadata.
