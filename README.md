# javascript-manuals

<img src="https://www.noroff.no/images/docs/vp2018/Noroff-logo_STDM_vertikal_RGB.jpg" alt="banner" width="450"/>

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

> Manuals and guides for the Noroff Accelerate AS Javascript course

## Table of Contents

- [Index](#index)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Index

1. [**HTML Basics**](html-basics/README.md) : An entry point into the web.

## Maintainers

[Greg Linklater (@EternalDeiwos)](https://gitlab.com/EternalDeiwos)

## Contributing

See [the contributing file](CONTRIBUTING.md)!

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2019 Noroff Accelerate AS
