# HTML

HTML _(HyperText Markup Language)_ is a language for rendering richly styled web pages.

The language is an extension of XML _(eXtensible Markup Language)_ that is designed to be interpreted and rendered in a web browser. Being extended from XML, HTML consists of definitions for various types of elements and behaviour for attributes on those types.

While HTML can be used in isolation without additional styles or scripts, it is further assisted by CSS3 and ECMAScript (a.k.a. Javascript), which may style and define programmed behaviour for components respectively. Even when not explicitly using these technologies, HTML, Javascript and CSS are inseparable and are nearly useless in isolation.

<img src="img/html-no-webpack.png" alt="Bare html page" width=600 />

## Elements & Tags

HTML is written in the style of XML and consists of encapsulating content in hierarchical trees using tags. Tags may be written in one of two ways:

### Start & End Tags

<img src="img/xml-tag-children.png" alt="XML tag with children" width=600 />

Elements that are intended to contain child elements (even if they currently don't contain any) are written with a **Start Tag** and an **End Tag**. The content between the start tag and the end tag is defined as being the children of the element, while the content within the start tag itself -- i.e. the tag type and attributes -- is the element itself. Failure to include an end tag may result in the HTML document being rendered incorrectly.

Both tags use **angle brackets** to denote the boundaries of the tag content, however the **End Tag** uses a forward slash (`/`) character to denote the end of the element. There are no limitations on the number of elements an element may contain.

```html
<b>         </b>
^^^         ^^^
start tag   end tag
```

### Self-encapsulating Elements

<img src="img/xml-tag-encapsulated.png" alt="XML tag with children" width=600 />

Elements that are not intended to contain child elements, such as the `<img />` tag, may be written in the self-encapsulating form. This is characterised by the **Start Tag** also containing the forward slash (`/`) character, as used in the **End Tag**, before the closing **angle bracket**. Failure to include the forward slash character to properly complete a self-encapsulating character may result in unintended results when rendering the HTML document.

```html
<img               />
^^^                ^^^
normal start tag   closing self-encapsulation
```

## Attributes

HTML elements may have additional metadata associated with them that changes their behaviour somehow, or are required for their primary behaviour; for example:

* The `src` attribute of an `<img />` element is required for the element to behave normally.
* The `disabled` attribute of an `<input />` element is not required but if present makes the input uneditable by the user.

The full list of attributes for a type of tag may be seen in the documentation for each tag from [MDN](#mdn). This document will discuss some of the common tags used across multiple elements:

### Style Attribute

<img src="img/html-style-attr.png" alt="HTML style attr" width=600 />

The `style` attribute is a common attribute to all HTML elements which can be used to specify CSS style rules that apply only to that element. Styles that are applied to elements in this way override all other styles inherited from CSS classes, those specified against the element ID, from the tag itself or from a parent component. This is both useful for testing or for functions that generate elements.

### Class Attribute

<img src="img/html-class-attr.png" alt="HTML class attr" width=600 />

The `class` attribute is a common attribute to all HTML elements which serves multiple purposes.

1. The classes defined in the class attribute can have CSS styles associated with them which are applied to those elements. As styles are intended to be reusable, especially those that are provided by frameworks such as [Bootstrap](https://getbootstrap.com/docs/4.1/getting-started/introduction/), this has naturally become the primary way to assign styles to HTML elements.
2. Classes also serve as a means of selecting elements out of the DOM using a query. A query such as this would return an array of elements which have the queried classes applied.

It can be used to specify zero or more CSS classes to apply to that element. The value of the class attribute should be a space-separated list of CSS classes (without the "`.`" selector).

### ID Attribute

<img src="img/html-id-attr.png" alt="HTML id attr" width=600 />

The `id` attribute is a common attribute to all HTML elements which serves multiple purposes:

1. The `id` primarily serves as a means of selecting a specific element out of the DOM.
2. The `id` can also have CSS styles associated with them which are applied to the corresponding element.

The value of the `id` attribute should be chosen carefully as it must be unique on the current HTML page

### Event Handler Attributes

`TODO`

## Dependencies

Importing dependencies into HTML web pages can be done in a few different ways, ranging from inline inclusion to inclusion from a remote resource.

### Style Element

<img src="img/html-style-elem.png" alt="HTML style element" width=600 />

The `<style></style>` element can be used to write CSS content inline with HTML for immediate use. While professional CSS styles can tend to be too extensive to embed directly into an HTML page, it may be meaningful to include some basic styles inline within the initial web page to normalise or establish a baseline style on which styles loaded later may be applied.

Inline styles may also be appropriate for limited HTML pages without extensive styles as it makes serving the web page easier to not have to serve a separate file along side it.

### Link Element

<img src="img/html-link-elem.png" alt="HTML link element" width=600 />

The `<link />` element is used to describe a relationship to an external document. Many kinds of website components are loaded with this element, including application icons, fonts and CSS files. Files referenced using this element can be addressed relatively on the local server, or external on a remote resource.

Program scripts (JS, Typescript, Coffeescript or other "web languages") are not referenced using the `<link />` element and are instead imported using the `<script></script>` element.

### Script Element

<img src="img/html-script-elem-inline.png" alt="HTML inline script element" width=600 />

<img src="img/html-script-elem.png" alt="HTML inline script element" width=600 />

Program scripts can be included, both inline and by reference, using the `<script></script>` tags.

The `type` attribute specifies the type of the script being included (inline or by reference) and by doing so specifies how the script should be interpreted. The browser can be extended to add interpreters for custom types that are not available natively in the browser environment. If the `type` is omitted then it will be assumed to be `text/javascript`.

The `src` attribute is used to specify a remote path for the script to be included from. The path may be referenced locally or from a remote resource. If the `src` attribute is specified then all text within the script tags will be ignored.

#### Script Element Caveat

While the `<script></script>` element is used to include program scripts from remote resources; it should be noted that it will not work if the self-encapsulating tag is used, even if the element is empty. The program script will not be executed and no error or warnings will be shown.

```html
<script src="index.js" type="text/javascript" />
                                             ^^^ WRONG!
```

### Webpack

All of the above methods of including remote resources extend to traditional, staticly built, HTML5, CSS3 and Javascript websites. When considering modern websites that utilize [NPM](https://npmjs.com), these websites often use a bundler to manage the transpilation, packaging and minification of a website. Webpack is one such popular bundler.

Webpack will be discussed in detail in a different guide, but for the purpose of disucssing including resources, the process differs from what is described above. With webpack, resources may be included by importing them from within the Javascript for that application, so long as there is a plugin to handle importing files of that type; SCSS files, for example, require a plugin to pre-compile them to CSS before they are bundled for distribution.

<img src="img/js-webpack-import.png" alt="Import JS with webpack" width=600 />

This is an example of ES6 importing where the imported scripts are assigned to a variable within the scope of the current module. Following these lines, the variables `$` and `moment` are bound to the libraries from NPM called `jquery` and `moment` respectively.

Behind the scenes (at build-time), in processing this import statement, webpack will add the modules retrieved from the import and include them in the bundle that is served to the client. No further effort needs to be made to ensure their presence at runtime.

<img src="img/js-webpack-import-css.png" alt="Import CSS with webpack" width=600 />

The same is true for CSS files. Common automatically generated Webpack configurations usually come with plugins for CSS and SCSS installed and configured by default. In the case of the latter, there is also an additional compilation step to compile SCSS into valid CSS for later rendering.

As with any other import, CSS and SCSS can be bound to a variable if that is desirable but as long as there is an import statement that correctly imports the referenced document then the styles defined within will be automatically applied to the rendered application at runtime. In most cases, it is not necessary to bind such imports to a variable.

<img src="img/js-webpack-import-npm.png" alt="Import CSS from NPM with webpack" width=600 />

Any valid path may be referenced. In this example, the import of a css file is shown from the module installed by NPM called `bootstrap`. The import is done as normal referencing the library by name but also includes the additional path information after the library's name. This import would resolve to the path: `<root>/node_modules/bootstrap/dist/css/bootstrap.min.css`.

<img src="img/js-webpack-import-png.png" alt="Import images with webpack" width=600 />

Importing is not only limited to text files; any kind of file can be imported, so long as there is a valid plugin for webpack to handle it. For a complete list of available "loader" plugins and how they work, see the [Webpack Documentation](https://webpack.js.org/loaders/).

## Development with HTML

Creating a web page with HTML is simple in the simplest cast but can get complicated very quickly. It would be best to see the full range of capabilities through the HTML tutorial provided by the [MDN](#mdn). For the purpose of the course, a select number of topics will be covered here.

# External Resources

## MDN

The Mozilla Developer Network (MDN) is an online resource that will help learn the basics for the web technologies we will use for this course.

### Element Types

HTML5 defines a number of tag types that have different effects on the rendering of the document in the browser. The full list of tags can be seen here: https://developer.mozilla.org/en-US/docs/Web/HTML/Element.

### HTML Reference

MDN provides a well structured reference document for HTML and web pages in general which can be seen here: https://developer.mozilla.org/en-US/docs/Web/HTML. This also includes some discussion on advanced topics such as [CORS](#cors) and [Events](#events).

### Comprehensive Tutorial

Everything you need to know about web programming can be found here: https://developer.mozilla.org/en-US/docs/Learn. Don't forget to ask questions if you don't understand something.
